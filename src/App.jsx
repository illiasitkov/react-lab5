import React, { useEffect } from 'react';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import './App.css';
import CourseForm from './components/CourseForm/CourseForm';
import { TOKEN_LOCAL_STORAGE } from './constants';
import { Registration } from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { fetchAuthors, fetchCourses } from './store/services';
import { useDispatch, useSelector } from 'react-redux';
import { coursesFetched } from './store/courses/actionCreators';
import { authorsFetched } from './store/authors/actionCreators';
import { userIsAuth } from './store/user/actionCreators';
import { getUserIsAdmin, getUserIsAuth } from './store/selectors';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';

function App() {
	const dispatch = useDispatch();

	const userIsAuthenticated = useSelector(getUserIsAuth);
	const userIsAdmin = useSelector(getUserIsAdmin);

	useEffect(() => {
		checkUserLoggedIn();
		loadAuthors().then(() => {
			loadCourses();
		});
	}, []);

	const checkUserLoggedIn = () => {
		const token = localStorage.getItem(TOKEN_LOCAL_STORAGE);
		if (token) {
			dispatch(userIsAuth(true));
		}
	};

	const loadCourses = async () => {
		try {
			const res = await fetchCourses();
			const obj = await res.json();
			if (res.status === 200) {
				dispatch(coursesFetched(obj.result));
			} else {
				alert(res.statusText);
			}
		} catch (e) {
			alert(e);
		}
	};

	const loadAuthors = async () => {
		try {
			const res = await fetchAuthors();
			const obj = await res.json();
			if (res.status === 200) {
				dispatch(authorsFetched(obj.result));
			} else {
				alert(res.statusText);
			}
		} catch (e) {
			alert(e);
		}
	};

	return (
		<div>
			<BrowserRouter>
				<Header />
				<Routes>
					{!userIsAuthenticated && (
						<>
							<Route path='/login' exact element={<Login />} />
							<Route path='/registration' exact element={<Registration />} />
							<Route path='*' element={<Navigate to='/login' />} />
						</>
					)}
					{!!userIsAuthenticated && (
						<>
							<Route exact path='/courses' element={<Courses />} />
							<Route path='/courses/:courseId' element={<CourseInfo />} />
							<Route
								path='/courses/add'
								exact={true}
								element={
									<PrivateRoute condition={userIsAdmin}>
										<CourseForm />
									</PrivateRoute>
								}
							/>
							<Route
								path='/courses/update/:courseId'
								element={
									<PrivateRoute condition={userIsAdmin}>
										<CourseForm updateMode={true} />
									</PrivateRoute>
								}
							/>
							<Route path='*' element={<Navigate to='/courses' />} />
						</>
					)}
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;
