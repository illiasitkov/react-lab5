import React from 'react';
import './Input.css';
import PropTypes from 'prop-types';

const Input = ({
	type,
	width = '',
	id,
	labelText,
	inputPlaceholder,
	onChange,
	value,
	min = 0,
}) => {
	return (
		<div>
			<label htmlFor={id}>{labelText}</label>
			{labelText && <br />}
			<input
				style={{ width }}
				required={true}
				id={id}
				type={type}
				value={value}
				onChange={onChange}
				placeholder={inputPlaceholder}
				min={min}
			/>
		</div>
	);
};

Input.propTypes = {
	type: PropTypes.oneOf([
		'text',
		'email',
		'password',
		'search',
		'tel',
		'number',
	]).isRequired,
	width: PropTypes.string.isRequired,
	id: PropTypes.string.isRequired,
	labelText: PropTypes.string,
	inputPlaceholder: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.any.isRequired,
	min: PropTypes.number.isRequired,
};

Input.defaultProps = {
	min: 0,
	width: '',
};

export default Input;
