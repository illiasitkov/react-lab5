import React from 'react';
import Button from '../../../../common/Button/Button';
import { ADD_AUTHOR, DELETE_AUTHOR } from '../../../../constants';
import PropTypes from 'prop-types';

const Author = ({ author, onDelete, onAdd, deleteMode }) => {
	return (
		<div className='d-flex flex-wrap justify-content-between align-items-center'>
			<span>{author.name}</span>
			<Button
				onClick={deleteMode ? onDelete : onAdd}
				buttonText={deleteMode ? DELETE_AUTHOR : ADD_AUTHOR}
			/>
		</div>
	);
};

Author.propTypes = {
	author: PropTypes.shape({
		name: PropTypes.string.isRequired,
	}).isRequired,
	onDelete: PropTypes.func,
	onAdd: PropTypes.func,
	deleteMode: PropTypes.bool,
};

Author.defaultProps = {
	deleteMode: false,
};

export default Author;
