import React from 'react';
import { fireEvent, render, screen, within } from '@testing-library/react';
import { Provider } from 'react-redux';
import { Route, MemoryRouter, Routes } from 'react-router-dom';
import CourseForm from '../CourseForm';

it('should show authors lists (all and course authors)', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
			{
				id: '1',
				name: 'Author 1',
			},
			{
				id: '2',
				name: 'Author 2',
			},
			{
				id: '3',
				name: 'Author 3',
			},
		],
		courses: [
			{
				id: '0',
				title: 'JavaScript',
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronictypesetting, remaining essentially unchanged.`,
				creationDate: '8/3/2021',
				duration: 160,
				authors: ['1', '3'],
			},
		],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<MemoryRouter initialEntries={['/0']}>
				<Routes>
					<Route
						path={'/:courseId'}
						element={<CourseForm updateMode={true} />}
					/>
				</Routes>
			</MemoryRouter>
		</Provider>
	);

	const [l1, l2] = screen.queryAllByTestId('authorList');

	expect(l1).toHaveTextContent('Author 0');
	expect(l1).toHaveTextContent('Author 2');

	expect(l2).toHaveTextContent('Author 1');
	expect(l2).toHaveTextContent('Author 3');
});

it("'Create author' click button should call dispatch", () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
			{
				id: '1',
				name: 'Author 1',
			},
			{
				id: '2',
				name: 'Author 2',
			},
			{
				id: '3',
				name: 'Author 3',
			},
		],
		courses: [
			{
				id: '0',
				title: 'JavaScript',
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronictypesetting, remaining essentially unchanged.`,
				creationDate: '8/3/2021',
				duration: 160,
				authors: ['1', '3'],
			},
		],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<MemoryRouter initialEntries={['/0']}>
				<Routes>
					<Route
						path={'/:courseId'}
						element={<CourseForm updateMode={true} />}
					/>
				</Routes>
			</MemoryRouter>
		</Provider>
	);

	fireEvent.change(screen.getByLabelText('Author name'), {
		target: { value: 'New Author' },
	});

	fireEvent.click(screen.getByText('Create author'));

	expect(mockedStore.dispatch).toHaveBeenCalled();
});

it("'Add author' button click should add an author to course authors list", () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
			{
				id: '1',
				name: 'Author 1',
			},
			{
				id: '2',
				name: 'Author 2',
			},
			{
				id: '3',
				name: 'Author 3',
			},
		],
		courses: [
			{
				id: '0',
				title: 'JavaScript',
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronictypesetting, remaining essentially unchanged.`,
				creationDate: '8/3/2021',
				duration: 160,
				authors: ['1', '3'],
			},
		],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<MemoryRouter initialEntries={['/0']}>
				<Routes>
					<Route
						path={'/:courseId'}
						element={<CourseForm updateMode={true} />}
					/>
				</Routes>
			</MemoryRouter>
		</Provider>
	);

	const btn = within(screen.getByText('Author 0').closest('div')).getByTestId(
		'button'
	);

	fireEvent.click(btn);

	const [l1, l2] = screen.queryAllByTestId('authorList');

	expect(l1).toHaveTextContent('Author 2');

	expect(l2).toHaveTextContent('Author 0');
	expect(l2).toHaveTextContent('Author 1');
	expect(l2).toHaveTextContent('Author 3');
});

it("'Delete author' button click should delete an author from the course list", () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
			{
				id: '1',
				name: 'Author 1',
			},
			{
				id: '2',
				name: 'Author 2',
			},
			{
				id: '3',
				name: 'Author 3',
			},
		],
		courses: [
			{
				id: '0',
				title: 'JavaScript',
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronictypesetting, remaining essentially unchanged.`,
				creationDate: '8/3/2021',
				duration: 160,
				authors: ['1', '3'],
			},
		],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<MemoryRouter initialEntries={['/0']}>
				<Routes>
					<Route
						path={'/:courseId'}
						element={<CourseForm updateMode={true} />}
					/>
				</Routes>
			</MemoryRouter>
		</Provider>
	);

	const btn = within(screen.getByText('Author 1').closest('div')).getByTestId(
		'button'
	);

	fireEvent.click(btn);

	const [l1, l2] = screen.queryAllByTestId('authorList');

	expect(l1).toHaveTextContent('Author 0');
	expect(l1).toHaveTextContent('Author 1');
	expect(l1).toHaveTextContent('Author 2');

	expect(l2).toHaveTextContent('Author 3');
});
