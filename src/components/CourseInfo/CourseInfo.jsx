import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import CourseDetails from '../Courses/components/CourseCard/components/CourseDetails/CourseDetails';

import './CourseInfo.css';
import { useSelector } from 'react-redux';
import { getCourses } from '../../store/selectors';

const CourseInfo = () => {
	const [course, setCourse] = useState(null);

	const params = useParams();

	const courses = useSelector(getCourses);

	useEffect(() => {
		setCourse(courses.filter((c) => c.id === params.courseId)[0]);
	}, []);

	return (
		<div className='content-wrapper'>
			<Link to='/courses'>{'< Back to courses'}</Link>
			{course ? (
				<>
					<h1 className='text-center'>{course.title}</h1>
					<div className='row'>
						<p className='col-6'>{course.description}</p>
						<div className='offset-1 col-5'>
							<CourseDetails showId={true} course={course} />
						</div>
					</div>
				</>
			) : (
				<h2>Loading...</h2>
			)}
		</div>
	);
};

export default CourseInfo;
