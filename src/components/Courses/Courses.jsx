import React, { useEffect, useState } from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { useNavigate } from 'react-router-dom';
import { BTN_ADD_COURSE } from '../../constants';
import { useDispatch, useSelector } from 'react-redux';
import { getCourses, getUserIsAdmin } from '../../store/selectors';
import { getCurrentUserThunk } from '../../store/user/thunk';

const Courses = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const [searchStr, setSearchStr] = useState('');

	const courses = useSelector(getCourses);
	const userIsAdmin = useSelector(getUserIsAdmin);

	useEffect(() => {
		loadCurrentUser();
	}, []);

	const loadCurrentUser = () => {
		dispatch(getCurrentUserThunk());
	};

	const filter = (string) => (course) => {
		let stringLower = string.toLowerCase();
		return (
			course.title.toLowerCase().includes(stringLower) ||
			course.id.toLowerCase().includes(stringLower)
		);
	};

	const navigateToCreateCourse = () => {
		navigate('/courses/add', { replace: true });
	};

	const views = courses.filter(filter(searchStr)).map((course) => {
		return (
			<div key={course.id} className='mb-4'>
				<CourseCard course={course} />
			</div>
		);
	});

	return (
		<section className='content-wrapper'>
			<div className='d-flex justify-content-between align-items-center mb-4 flex-wrap'>
				<SearchBar onSearch={setSearchStr} />
				{userIsAdmin && (
					<Button
						buttonText={BTN_ADD_COURSE}
						onClick={navigateToCreateCourse}
					/>
				)}
			</div>
			<div data-testid='coursesContainer'>{views}</div>
		</section>
	);
};

export default Courses;
