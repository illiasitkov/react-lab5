import React from 'react';
import './CourseCard.css';
import Button from '../../../../common/Button/Button';
import CourseDetails from './components/CourseDetails/CourseDetails';
import { SHOW_COURSE } from '../../../../constants';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { getUserIsAdmin, getUserToken } from '../../../../store/selectors';
import { courseDeletedThunk } from '../../../../store/courses/thunk';

const CourseCard = ({ course }) => {
	const navigate = useNavigate();

	const dispatch = useDispatch();
	const token = useSelector(getUserToken);

	const userIsAdmin = useSelector(getUserIsAdmin);

	const deleteCourse = () => {
		dispatch(courseDeletedThunk(course.id, token));
	};

	const updateCourse = () => {
		navigate(`/courses/update/${course.id}`, { replace: true });
	};

	const navigateToCourse = () => {
		navigate(`/courses/${course.id}`, { replace: true });
	};

	return (
		<div data-testid='courseCard' className='course-card'>
			<div className='row'>
				<div className='col-8'>
					<h2 className='mb-3'>
						<strong>{course.title}</strong>
					</h2>
					<div>{course.description}</div>
				</div>
				<div className='col-4'>
					<CourseDetails course={course} showId={false} />
					<div className='mt-4 d-flex gap-2 justify-content-center flex-wrap'>
						<Button buttonText={SHOW_COURSE} onClick={navigateToCourse} />
						{userIsAdmin && (
							<>
								<Button icon={true} onClick={updateCourse}>
									<svg
										xmlns='http://www.w3.org/2000/svg'
										width='22'
										height='22'
										fill='currentColor'
										className='bi bi-pencil-fill'
										viewBox='0 0 16 16'
									>
										<path d='M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z' />
									</svg>
								</Button>
								<Button icon={true} onClick={deleteCourse}>
									<svg
										xmlns='http://www.w3.org/2000/svg'
										width='24'
										height='24'
										fill='currentColor'
										className='bi bi-trash3-fill'
										viewBox='0 0 16 16'
									>
										<path d='M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z' />
									</svg>
								</Button>
							</>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	course: PropTypes.shape({
		id: PropTypes.string.isRequired,
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
	}).isRequired,
};

export default CourseCard;
