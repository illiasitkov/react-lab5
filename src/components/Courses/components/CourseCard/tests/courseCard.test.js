import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';
import CourseCard from '../CourseCard';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

afterEach(cleanup);

it('should display title', () => {
	const mockedCourse = {
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum description`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [],
	};

	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [],
		courses: [mockedCourse],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CourseCard course={mockedCourse} />
			</BrowserRouter>
		</Provider>
	);

	expect(screen.queryByText(mockedCourse.title)).toBeInTheDocument();
});

it('should display description', () => {
	const mockedCourse = {
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum description`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [],
	};

	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [],
		courses: [mockedCourse],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CourseCard course={mockedCourse} />
			</BrowserRouter>
		</Provider>
	);

	expect(screen.queryByText(mockedCourse.description)).toBeInTheDocument();
});

it('should display authors list', () => {
	const mockedCourse = {
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum description`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: ['3', '0', '2', '1'],
	};

	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
			{
				id: '1',
				name: 'Author 1',
			},
			{
				id: '2',
				name: 'Author 2',
			},
			{
				id: '3',
				name: 'Author 3',
			},
		],
		courses: [mockedCourse],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CourseCard course={mockedCourse} />
			</BrowserRouter>
		</Provider>
	);

	mockedCourse.authors.forEach((authorId) => {
		const author = mockedState.authors.find((a) => a.id === authorId);
		const regexp = new RegExp(author.name, 'im');
		expect(screen.queryByText(regexp)).toBeInTheDocument();
	});
});

it('should display duration in the correct format', () => {
	const mockedCourse = {
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum description`,
		creationDate: '8/3/2021',
		duration: 561,
		authors: [],
	};

	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [],
		courses: [mockedCourse],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CourseCard course={mockedCourse} />
			</BrowserRouter>
		</Provider>
	);

	expect(screen.queryByText(/hours/)).toHaveTextContent(/\s*09:21\s+hours\s*/);
});

it('should display created date in the correct format', () => {
	const mockedCourse = {
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum description`,
		creationDate: '8/1/2021',
		duration: 561,
		authors: [],
	};

	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [],
		courses: [mockedCourse],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CourseCard course={mockedCourse} />
			</BrowserRouter>
		</Provider>
	);
	expect(screen.queryByText(/\s*08\.01\.2021\s*/)).toBeInTheDocument();
});
