import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Courses from '../Courses';
import App from '../../../App';

it('should display amount of CourseCard equal length of courses array', () => {
	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
		],
		courses: [
			{
				id: '0',
				title: 'JavaScript 0',
				description: `Lorem Ipsum description`,
				creationDate: '18/1/2021',
				duration: 359,
				authors: ['0'],
			},
			{
				id: '1',
				title: 'JavaScript 1',
				description: `Lorem Ipsum description`,
				creationDate: '8/10/2021',
				duration: 93,
				authors: ['0'],
			},
			{
				id: '2',
				title: 'JavaScript 2',
				description: `Lorem Ipsum description`,
				creationDate: '8/11/2021',
				duration: 345,
				authors: ['0'],
			},
			{
				id: '3',
				title: 'JavaScript 3',
				description: `Lorem Ipsum description`,
				creationDate: '8/11/2022',
				duration: 123,
				authors: ['0'],
			},
		],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Courses />
			</BrowserRouter>
		</Provider>
	);

	expect(screen.queryAllByTestId('courseCard')).toHaveLength(
		mockedState.courses.length
	);
});

it('should display Empty container if courses array length is 0', () => {
	const mockedState = {
		user: {
			token: 's0me_t0ken',
			role: 'REGULAR',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
		],
		courses: [],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Courses />
			</BrowserRouter>
		</Provider>
	);
	expect(mockedState.courses).toHaveLength(0);
	expect(screen.getByTestId('coursesContainer')).toBeEmptyDOMElement();
});

it('CourseForm should be shown after a click on a button "Add new course"', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		authors: [
			{
				id: '0',
				name: 'Author 0',
			},
		],
		courses: [],
	};

	const mockedStore = {
		dispatch: jest.fn(),
		subscribe: jest.fn(),
		getState: () => mockedState,
	};

	render(
		<Provider store={mockedStore}>
			<App />
		</Provider>
	);

	fireEvent.click(screen.getByText('Add new course'));
	expect(screen.queryByTestId('courseForm')).toBeInTheDocument();
});
