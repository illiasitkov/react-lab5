import React from 'react';
import { LOGO_ALT } from '../../../../constants';
import { useNavigate } from 'react-router-dom';

export const Logo = () => {
	const navigate = useNavigate();

	const navigateToAllCourses = () => {
		navigate('/courses', { replace: true });
	};
	return (
		<img
			data-testid='logo'
			style={{ cursor: 'pointer' }}
			width='48px'
			src='https://upload.wikimedia.org/wikipedia/commons/d/db/Zeronet_logo.png'
			alt={LOGO_ALT}
			onClick={navigateToAllCourses}
		/>
	);
};
