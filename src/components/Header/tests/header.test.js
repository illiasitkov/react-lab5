import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import Header from '../Header';
import { BrowserRouter } from 'react-router-dom';

it("should have logo and user's name", () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
		courses: [],
		authors: [],
	};

	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Header />
			</BrowserRouter>
		</Provider>
	);
	expect(screen.queryByText(mockedState.user.name)).toBeInTheDocument();
	expect(screen.queryByTestId('logo')).toBeInTheDocument();
});
