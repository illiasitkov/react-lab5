import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Input from '../../common/Input/Input';
import { changeHandler } from '../../helpers/changeHandler';
import Button from '../../common/Button/Button';
import { loginUser } from '../../services/AuthService';
import { useDispatch } from 'react-redux';
import { fetchedUser } from '../../store/user/actionCreators';
import { TOKEN_LOCAL_STORAGE } from '../../constants';

const Login = () => {
	const dispatch = useDispatch();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const login = async (e) => {
		e.preventDefault();
		try {
			const res = await loginUser(email, password);
			const obj = await res.json();
			if (res.status !== 201) {
				alert('Login failed');
			} else {
				const token = obj.result;
				const { email, name } = obj.user;
				localStorage.setItem(TOKEN_LOCAL_STORAGE, token);
				dispatch(fetchedUser(name, email, token));
			}
		} catch (err) {
			alert(err);
		}
	};

	return (
		<div className='row content-wrapper'>
			<div className='mt-5 offset-md-4 offset-2 col-md-4 col-8'>
				<h3 className='text-center mb-2'>Login</h3>
				<form onSubmit={login}>
					<Input
						onChange={changeHandler(setEmail)}
						value={email}
						width='100%'
						id='email'
						inputPlaceholder='Enter email'
						labelText='Email'
						type='email'
					/>
					<Input
						onChange={changeHandler(setPassword)}
						value={password}
						width='100%'
						id='password'
						inputPlaceholder='Enter password'
						labelText='Password'
						type='password'
					/>
					<div className='d-flex flex-column align-items-center gap-2'>
						<Button type='submit' buttonText='Login' />
						<p>
							If you don't have an account you can&nbsp;
							<Link to='/registration'>Register</Link>
						</p>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Login;
