import React from 'react';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const PrivateRoute = ({ condition, children }) => {
	if (condition) {
		return children;
	} else {
		return <Navigate to='/courses' />;
	}
};

PrivateRoute.propTypes = {
	condition: PropTypes.bool.isRequired,
};

export default PrivateRoute;
