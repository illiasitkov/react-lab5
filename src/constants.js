export const LOGO_ALT = 'Logo image';
export const USERNAME = 'John Doe';
export const LOGOUT = 'Logout';
export const BTN_ADD_COURSE = 'Add new course';
export const AUTHORS = 'Authors';
export const DURATION = 'Duration';
export const CREATED = 'Created';
export const SHOW_COURSE = 'Show course';
export const BTN_SEARCH = 'Search';
export const SEARCH_PLACEHOLDER = 'Enter course name...';
export const FILL_FIELDS_ALERT = 'Please, fill in all the fields';
export const TITLE = 'Title';
export const TITLE_PLACEHOLDER = 'Enter title...';
export const DESCRIPTION = 'Description';
export const DESCRIPTION_PLACEHOLDER = 'Enter description';
export const COURSE_AUTHORS = 'Course authors';
export const ALL_COURSES = 'All courses';
export const DELETE_AUTHOR = 'Delete author';
export const ADD_AUTHOR = 'Add author';

export const TOKEN_LOCAL_STORAGE = 'user_token';
