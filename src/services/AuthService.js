const url = 'http://localhost:4000';

export const registerNewUser = (name, email, password) => {
	const newUser = {
		name,
		email,
		password,
	};

	return fetch(`${url}/register`, {
		method: 'POST',
		body: JSON.stringify(newUser),
		headers: {
			'Content-Type': 'application/json',
		},
	});
};

export const loginUser = (email, password) => {
	const user = { email, password };

	return fetch(`${url}/login`, {
		method: 'POST',
		body: JSON.stringify(user),
		headers: {
			'Content-Type': 'application/json',
		},
	});
};

export const logoutUser = (bearerToken) => {
	return fetch(`${url}/logout`, {
		method: 'DELETE',
		headers: {
			Authorization: bearerToken,
		},
	});
};
