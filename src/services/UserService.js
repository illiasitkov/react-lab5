const url = 'http://localhost:4000/users/me';

export const loadUser = (bearerToken) => {
	return fetch(url, {
		method: 'GET',
		headers: {
			Authorization: bearerToken,
		},
	});
};
