import { authorAdded } from './actionCreators';
import { createAuthor } from '../services';

export const authorAddedThunk = (name, token) => async (dispatch) => {
	try {
		const res = await createAuthor(name, token);
		if (res.status === 201) {
			const obj = await res.json();
			dispatch(authorAdded(obj.result));
		} else {
			alert(res.status + ' ' + res.statusText);
		}
	} catch (e) {
		console.log(e);
	}
};
