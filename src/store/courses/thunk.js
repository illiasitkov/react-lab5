import { createCourse, deleteCourse, updateCourse } from '../services';
import { courseAdded, courseDeleted, courseUpdated } from './actionCreators';

export const courseDeletedThunk =
	(courseId, bearerToken) => async (dispatch) => {
		try {
			const res = await deleteCourse(courseId, bearerToken);
			if (res.status === 200) {
				dispatch(courseDeleted(courseId));
			} else {
				alert(res.status + ' ' + res.statusText);
			}
		} catch (e) {
			console.log(e);
		}
	};

export const courseUpdatedThunk = (course, token) => async (dispatch) => {
	try {
		const res = await updateCourse(course, token);
		if (res.status === 200) {
			const obj = await res.json();
			dispatch(courseUpdated(obj.result));
		} else {
			alert(res.status + ' ' + res.statusText);
		}
	} catch (e) {
		console.log(e);
	}
};

export const courseAddedThunk = (course, token) => async (dispatch) => {
	try {
		const res = await createCourse(course, token);
		if (res.status === 201) {
			const obj = await res.json();
			dispatch(courseAdded(obj.result));
		} else {
			alert(res.status + ' ' + res.statusText);
		}
	} catch (e) {
		console.log(e);
	}
};
