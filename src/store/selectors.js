import { UserRoles } from './user/userRoles';

export const getCourses = (state) => state.courses;
export const getCourseById = (courseId) => (state) =>
	state.courses.find((c) => c.id === courseId);

export const getAuthors = (state) => state.authors;

export const getUser = (state) => state.user;
export const getUserIsAdmin = (state) => state.user.role === UserRoles.ADMIN;
export const getUserIsAuth = (state) => state.user.isAuth;
export const getUserToken = (state) => state.user.token;
