import { rootReducer } from '../index';
import { courseAdded, coursesFetched } from '../courses/actionCreators';

it('should return the initial state', () => {
	const state = rootReducer(undefined, {});
	expect(state).toEqual({
		user: {
			isAuth: false,
			name: '',
			email: '',
			token: '',
			role: '',
		},
		courses: [],
		authors: [],
	});
});

it('should handle SAVE_COURSE and returns new state', () => {
	const oldState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		courses: [],
		authors: [
			{
				id: '0',
				name: 'Author',
			},
		],
	};

	const newCourse = {
		title: 'New Course',
		description: 'Description',
		duration: 123,
		authors: ['0'],
	};

	const state = rootReducer(oldState, courseAdded(newCourse));
	expect(state.courses).toContainEqual(newCourse);
});

it('should handle GET_COURSES and returns new state', () => {
	const oldState = {
		user: {
			isAuth: true,
			name: 'John',
			email: 'email',
			token: 'token',
			role: 'ADMIN',
		},
		courses: [],
		authors: [
			{
				id: '0',
				name: 'Author',
			},
		],
	};

	const courses = [
		{
			id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
			title: 'JavaScript',
			description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronictypesetting, remaining essentially unchanged.`,
			creationDate: '8/3/2021',
			duration: 160,
			authors: ['0'],
		},
		{
			id: 'b5630fdd-7bf7-4d39-b75a-2b5906fd0916',
			title: 'Angular',
			description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
			creationDate: '10/11/2020',
			duration: 210,
			authors: ['0'],
		},
	];

	const state = rootReducer(oldState, coursesFetched(courses));
	expect(state.courses).toEqual(courses);
});
