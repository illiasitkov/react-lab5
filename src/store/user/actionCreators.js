import * as actionTypes from './actionTypes';

export const fetchedUser = (name, email, token, role) => ({
	type: actionTypes.FETCHED_USER,
	payload: {
		name,
		email,
		token,
		role,
	},
});

export const userIsAuth = (isAuth) => ({
	type: actionTypes.USER_IS_AUTH,
	payload: {
		isAuth,
	},
});

export const userLoggedOut = () => ({
	type: actionTypes.USER_LOGGED_OUT,
});
