export const FETCHED_USER = 'user/fetched';
export const USER_IS_AUTH = 'user/isAuth';
export const USER_LOGGED_OUT = 'user/loggedOut';
